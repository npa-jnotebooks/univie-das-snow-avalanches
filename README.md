# UNIVIE-DAS-SNOW-AVALANCHES

Jupyter Notebooks for the analysis of Distributed Acoustic Sensing data recorded in Lech, aimed to define snow properties and snow-avalanches geometry

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Funivie-das-snow-avalanches/HEAD)